package com.innovecture.rahul.weatherapi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WeatherApiMain {

	public static void main(String[] args) {
		try {
			// This is hardcoded to get the response for getting weather for
			// City: LasVegas
			// Zip: 89110
			// Starting time 13 hours
			// Ending time: 17 hours
			// Use this URL to create your own api call 
			URL url = new URL("http://localhost:8080/weatherapi/webapi/temperature/89110?start=13&end=17");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				System.out.println();
			}
			
			conn.disconnect();
			
	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	  }
	}

}
