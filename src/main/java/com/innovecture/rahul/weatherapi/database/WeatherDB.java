package com.innovecture.rahul.weatherapi.database;

import java.util.HashMap;

public class WeatherDB {

	private static HashMap<String, String> cityAndPin = new HashMap<String, String>();
	
	public static HashMap<String, String> getCityAndPin(){
		
		cityAndPin.put("90011", "Los Angeles");
		cityAndPin.put("60629", "Brooklyn NY");
		cityAndPin.put("10025", "New York NY");
		cityAndPin.put("08701", "Lakewood");
		cityAndPin.put("92704", "Santa Ana");
		cityAndPin.put("89110", "Las Vegas");
		cityAndPin.put("95076", "Watsonville");
		cityAndPin.put("92154", "San Diego");
		cityAndPin.put("30349", "Atlanta");
		cityAndPin.put("75211", "Dallas");
		
		
		return cityAndPin;
	}
	
}
