package com.innovecture.rahul.weatherapi.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityDayWeather {
	
	private String cityName;
	private String zipCode;
	private ArrayList<String> temperature;
	
	public CityDayWeather() {
		
	}
	
	public CityDayWeather(String cityName, String zipCode, ArrayList<String> temperature) {
		super();
		this.cityName = cityName;
		this.zipCode = zipCode;
		this.temperature = temperature;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public ArrayList<String> getTemperature() {
		return temperature;
	}

	public void setTemperature(ArrayList<String> temperature) {
		this.temperature = temperature;
	}
	
	

}
