package com.innovecture.rahul.weatherapi.model;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Weather {
	
	private String zipCode;
	private String city;
	private HashMap<Integer, Integer> tempByHour;
	
	public Weather() { 
		
	}
	
	public Weather(String zipCode, String city, HashMap<Integer, Integer> tempByHour) {
		super();
		this.zipCode = zipCode;
		this.city = city;
		this.tempByHour = tempByHour;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public HashMap<Integer, Integer> getTempByHour() {
		return tempByHour;
	}


	public void setTempByHour(HashMap<Integer, Integer> tempByHour) {
		this.tempByHour = tempByHour;
	}
	
	

}
