package com.innovecture.rahul.weatherapi.resources;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.innovecture.rahul.weatherapi.model.CityDayWeather;
import com.innovecture.rahul.weatherapi.model.Weather;
import com.innovecture.rahul.weatherapi.service.WeatherService;

@Path("/temperature")
public class TemperatureInfo {
	/**
	 * This is HTTP GET method of retrieving all the cities and their temperature for the whole day
	 * 
	 * @return List of Weather objects
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Weather> getTemperatureOfAllCities() {
		WeatherService weatherService = new WeatherService();
		return weatherService.getWeather();
	}
	
	/**
	 * This is HTTP GET method for retrieving temperature for the day for a particular city by zip code
	 * Optional start time and end time can be also provided with this
	 * 
	 * @param zipCode - Zip-code of the city
	 * @param startTime - Starting time (Optional)
	 * @param endTime - Ending time (Optional)
	 * @return 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{zipCode}")
	public CityDayWeather getTemperatureByZip(@PathParam("zipCode") String zipCode, @QueryParam("start") int startTime, @QueryParam("end") int endTime){
		WeatherService weatherService = new WeatherService();
		return weatherService.getTempByZipSvc(zipCode, startTime, endTime);
	}
	

}
