package com.innovecture.rahul.weatherapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import com.innovecture.rahul.weatherapi.database.WeatherDB;
import com.innovecture.rahul.weatherapi.model.CityDayWeather;
import com.innovecture.rahul.weatherapi.model.Weather;

public class WeatherService {
	
	private HashMap<String, String> cityAndPin = WeatherDB.getCityAndPin();
	protected int min = 75;
	protected int max = 90;
	
	/**
	 * This method is used to get all the temperatures for the day for the all cities of USA in the DB
	 * Random function is used to generate the temperatues randomly
	 * @return List of Weather object
	 */
	public ArrayList<Weather> getWeather() {
		ArrayList<Weather> weathers = new ArrayList<Weather>();

		HashMap<Integer, Integer> tempByHour = getTempByHourMap();
		Weather weather;
		for (Entry<String, String> entry : cityAndPin.entrySet()) {
			weather = new Weather(entry.getKey(), entry.getValue(), tempByHour);
			weathers.add(weather);
		}
		return weathers;
		
	}
	
	/**
	 * This method is used to get Temperature for the day for the given ZIP CODE of the city. startTime and endTime are optional paramters,
	 * which if provided gives the temperature for that particular range
	 * @param zipCode
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public CityDayWeather getTempByZipSvc(String zipCode, int startTime, int endTime) {
		if(cityAndPin.containsKey(zipCode)) {
			try {
				if(startTime > 0 && endTime > 0 && startTime <=24 && endTime <=24 && startTime < endTime) {
					String cityName = cityAndPin.get(zipCode);
					ArrayList<String> temperature = new ArrayList<String>();
					for(int i=startTime; i<=endTime; i++) {
						temperature.add("Hour: "+i +":00 will have temperature: "+getRandomNumberInRange(75, 90)+"F");
					}
					return new CityDayWeather(cityName, zipCode, temperature);
				} else {
					String cityName = cityAndPin.get(zipCode);
					ArrayList<String> temperature = new ArrayList<String>();
					for(int i=1; i<=24; i++) {
						temperature.add("Hour: "+i +":00 will have temperature: "+getRandomNumberInRange(75, 90)+"F");
					}
					return new CityDayWeather(cityName, zipCode, temperature);
				}
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		// ToDo: Handle Exception
		return null;
	}
	
	private HashMap<Integer, Integer> getTempByHourMap (){
		HashMap<Integer, Integer> tempByHour = new HashMap<Integer, Integer>();
		
		for(int i=1; i<=24; i++) {
			tempByHour.put(i, getRandomNumberInRange(min, max));
		}
		return tempByHour;
	}
	
	private int getRandomNumberInRange(int min, int max) {
		
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
		
	}
}
