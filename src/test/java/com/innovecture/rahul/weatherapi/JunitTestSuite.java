package com.innovecture.rahul.weatherapi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.innovecture.rahul.weatherapi.resources.TemperatureInfoTest;
import com.innovecture.rahul.weatherapi.service.WeatherServiceTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   TemperatureInfoTest.class,
   WeatherServiceTest.class
})
public class JunitTestSuite {

}
