package com.innovecture.rahul.weatherapi.resources;

import static io.restassured.RestAssured.get;
import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class TemperatureInfoTest {
	
	@BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }
	
	/**
	 * This is the test to check the retrieval of all cities and its temperature
	 */
	@Test
    public void testAllCity() {
        get("weatherapi/webapi/temperature")
        .then()
        .statusCode(200);	
    }
	
	/**
	 * This is the test to check the retrieval of a city by its zip code.
	 */
	@Test
    public void testCityByZip() {
        get("weatherapi/webapi/temperature/89110")
        .then()
        .statusCode(200)
        .body("cityName", equalTo("Las Vegas"))
        .body("zipCode", equalTo("89110")); 	
    }
	
	
}
