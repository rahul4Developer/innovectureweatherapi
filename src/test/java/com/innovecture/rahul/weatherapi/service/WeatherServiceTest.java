package com.innovecture.rahul.weatherapi.service;


import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.innovecture.rahul.weatherapi.model.CityDayWeather;
import com.innovecture.rahul.weatherapi.model.Weather;

public class WeatherServiceTest {
	
	@Test
	public void getWeatherTest() {
		WeatherService weatherService = new WeatherService();
		ArrayList<Weather> weathers = weatherService.getWeather();
		Assert.assertNotNull("List shouldn't be null", weathers);
		Assert.assertEquals("wrong size", 10, weathers.size());
	}
	
	@Test
	public void getTempByZipSvc() {
		WeatherService weatherService = new WeatherService();
		CityDayWeather cityDayWeather = weatherService.getTempByZipSvc("89110", 0, 0);
		Assert.assertNotNull(cityDayWeather);
		Assert.assertEquals("Las Vegas", cityDayWeather.getCityName());
	}

}
